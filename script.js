const master =  document.getElementById("master")
//for randomizing
function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}

let images = JSON.parse(images_data)
images = shuffle(images)

let load = function(k){
	let limit = k < images.length ? k : images.length
	for(let i=0;i<limit;i++){
		im = images.pop()

		let aphoto = document.createElement("div");
		aphoto.classList.add("aphoto");

		let aimg = document.createElement("img");
		aimg.setAttribute("abigphotolink",im.image);
		aimg.setAttribute('asize',im.w+"x"+im.h)
		aimg.setAttribute("data-src",im.thumbnail);
		aimg.src = "images/proxy/proxy.png";
		aphoto.appendChild(aimg);


		master.appendChild(aphoto)
	}
	if(images.length==0){ document.querySelectorAll("a").forEach(function(element){element.remove()}) }
	observer.observe();
}



//lozad.js activating
let animes = ['aslideup', 'aslideup2', 'aslidedown2', 'aslidedown', 'aflash', 'aflash2', 'arighttop', 'arighttop2', 'aleftbottom', 'aleftbottom2'];
if(window.innerWidth < 600){
animes = ['aslideup', 'aslideup2', 'aslidedown2', 'aslidedown', 'aflash', 'aflash2'];
}
animes = shuffle(animes);
let i = 0;
observer = lozad("img", {
	load: function(el) {
		if (el.getAttribute('data-src')) {
			let newelement = new Image();
			newelement.src=el.getAttribute('data-src');
			newelement.addEventListener('load', function(){
				el.classList.add(animes[i++%animes.length]);
				window.setTimeout(()=>{
					el.src = el.getAttribute('data-src');
				},200);
			}, false);
		}
	}
});
