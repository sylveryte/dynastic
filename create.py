import os, json
from PIL import Image
if __name__ == "__main__":
    size = 400,400
    image_data=[]
    for x in os.listdir('./images'):
        d={}
        y = os.path.join('images',x)
        im = Image.open(y)
        w,h = im.size
        im.thumbnail(size)
        z = os.path.join('thumbnails',x)
        im.save(z)
        d['image']=y
        d['thumbnail']=z
        d['w']=w
        d['h']=h
        image_data.append(d)
    with open("images_data.json", "w") as fp:
        jdata = json.dumps(image_data)
        print("images_data='"+jdata+"';",file=fp)
